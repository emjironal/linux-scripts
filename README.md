# Useful linux scripts

## mute: mutes the headset microphone

`mute 1`

`mute 0`

## brightness: adjusts screen brightness to [0,100]%

`sudo brightness 50`

`sudo brightness 100`

## brightness-level: increases the current brightness by [-100,100+]%

`sudo brightness 5`

`sudo brightness -5`

## volume (requires libnotify)

`volume -v 20` sets the volume of the pulseaudio sink 0 to 20%

`volume -a 2` increments the current volume by 2% of the pulseaudio sink 0

`volume -a -2` decreases the current volume by 2% of the pulseaudio sink 0

## volume-up (requires libnotify)

`volume-up 2`  increments the current volume by 2% of the pulseaudio sink 0

`volume-up -2`  decreases the current volume by 2% of the pulseaudio sink 0

## volume-set (requires libnotify)

`volume-set 20` sets the volume of the pulseaudio sink 0 to 20%

## volume-mute (requires libnotify)

`volume-mute` mute/unmute the pulseaudio sink 0